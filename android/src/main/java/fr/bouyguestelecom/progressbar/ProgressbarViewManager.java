package fr.bouyguestelecom.progressbar;

import android.app.ActionBar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

/**
 * Created by lmenness on 08/02/2017.
 */

public class ProgressbarViewManager extends SimpleViewManager<ProgressBar> {

    public static final String REACT_CLASS = "Progressbar";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected ProgressBar createViewInstance(
            ThemedReactContext reactContext) {

        ProgressBar progressbar = new ProgressBar(reactContext, null, R.attr.progressbar_style);
        progressbar.setIndeterminate(true);
        progressbar.setMax(100);
        progressbar.setProgress(20);
        progressbar.setSecondaryProgress(50);

        /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,30);
        progressbar.setLayoutParams(params);*/

                /*
        <ProgressBar
        android:theme="@style/AppLinearProgress"
        style="@style/Widget.AppCompat.ProgressBar.Horizontal"
        android:indeterminate="true"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight=".50"
        android:max="100"
        android:progress="20"
        android:secondaryProgress="50" />*/


        return progressbar;
    }

    @ReactProp(name = "progress", defaultInt = 0)
    public void setProgress(ProgressBar view, int progress) {
        view.setProgress(progress);
    }

    @ReactProp(name = "indeterminate",
            defaultBoolean = false)
    public void setIndeterminate(ProgressBar view,
                                 boolean indeterminate) {
        view.setIndeterminate(indeterminate);
    }
}