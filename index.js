import { PropTypes } from 'react';
import { requireNativeComponent, View } from 'react-native';

var iface = {
  name: 'Progressbar',
  propTypes: {
    progress: PropTypes.number,
    indeterminate: PropTypes.bool,
    ...View.propTypes
  },
};

export default requireNativeComponent('Progressbar', iface);
